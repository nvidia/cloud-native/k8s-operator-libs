# The `operator-libs` project

This respository holds a collection of go packages to ease the development of
NVIDIA Operators for GPU/NIC management.

## DEPRECATION WARNING

This repository has moved to GitHub under [NVIDIA/k8s-operator-libs](https://github.com/NVIDIA/k8s-operator-libs) and
this repository will be archived. Further development will happen in GitHub.
